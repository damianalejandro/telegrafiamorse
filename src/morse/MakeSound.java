/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;


/**
 *
 * @author Adminlan
 */
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class MakeSound {

    public void beepCorto(long sleep) throws InterruptedException, URISyntaxException{
        try
        {
            InputStream audioSrc = getClass().getResourceAsStream("/archivos/corto.wav");
            InputStream bufferedIn = new BufferedInputStream(audioSrc);
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
            Thread.sleep(sleep);
            Clip clip = AudioSystem.getClip();
            clip.open(audioStream);
            clip.start();
            Thread.sleep(sleep);
        }
        catch (InterruptedException | LineUnavailableException | UnsupportedAudioFileException | IOException exc)
        {
            exc.printStackTrace(System.out);
        }
    }
    public void beepLargo(long sleep) throws InterruptedException, URISyntaxException{
        try
        {
            InputStream audioSrc = getClass().getResourceAsStream("/archivos/largo.wav");
            InputStream bufferedIn = new BufferedInputStream(audioSrc);
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
            Thread.sleep(sleep);
            Clip clip = AudioSystem.getClip();
            clip.open(audioStream);
            clip.start();
            Thread.sleep(sleep);
        }
        catch (InterruptedException | LineUnavailableException | UnsupportedAudioFileException | IOException exc)
        {
            exc.printStackTrace(System.out);
        }
    }
}