/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.net.URISyntaxException;

/**
 *
 * @author Adminlan
 */
public class codificacion {
    
    public void caracter (char c, long sleep, long sleepEspacio) throws InterruptedException, URISyntaxException{
    MakeSound sound = new MakeSound();
    if (c=='a' || c=='A'){ //A
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='b' || c=='B'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='c' || c=='C'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='d' || c=='D'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='e' || c=='E'){
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='f' || c=='F'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='g' || c=='G'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='h' || c=='H'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='i' || c=='I'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='j' || c=='J'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='k' || c=='K'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='l' || c=='L'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='m' || c=='M'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='n' || c=='N'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='o' || c=='O'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='p' || c=='P'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='q' || c=='Q'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='r' || c=='R'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='s' || c=='T'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='t' || c=='T'){
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='u' || c=='U'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='v' || c=='V'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='w' || c=='W'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='x' || c=='X'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='y' || c=='Y'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='z' || c=='Z'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='1'){
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='2'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='3'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='4'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c=='5'){
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='6'){
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='7'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='8'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='9'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepCorto(sleep);
        pausa(sleep);
    }else if (c=='0'){
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        sound.beepLargo(sleep);
        pausa(sleep);
    }else if (c==' '){
        pausa(sleepEspacio);
    }else{
        return;
    }
    }
    public void pausa (double sleep) throws InterruptedException{
        Thread.sleep((long) (sleep/1.5));
    }
}
